<?php 

// Get IP address
$ip_address = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR') ?: getenv('HTTP_X_FORWARDED') ?: getenv('HTTP_FORWARDED_FOR') ?: getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');

// Get JSON object
$jsondata = file_get_contents("http://timezoneapi.io/api/ip/?" . $ip_address);

// Decode
$data = json_decode($jsondata, true);

echo "<pre>"; print_r($data);

// Request OK?
if($data['meta']['code'] == '300'){

    // Example: Get the city parameter
    echo "City: " . $data['data']['city'] . "<br>";
	
	// Example : Get the country parameter
	echo "Country : ". $data['data']['country']."<br/>";
	
	
	// Example : Get the state parameter
	
	echo "State : ". $data['data']['state']."<br/>";

    // Example: Get the users time
    echo "Time: " . $data['data']['datetime']['date_time_txt'] . "<br>";
	
	// Example: Get the users timezone
    echo "Timezone: " . $data['data']['timezone']['id'] . "<br>";

}

?>